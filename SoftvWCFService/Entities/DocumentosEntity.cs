﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace Softv.Entities
{
    [DataContract]
    [Serializable]
    public class DocumentosEntity
    {
        [DataMember]
        public  String PDF { get; set; }

        [DataMember]
        public string Ticket { get; set; }

        [DataMember]
        public string Fecha { get; set; }

        [DataMember]
        public decimal ? Importe { get; set; }

        [DataMember]
        public String XML { get; set; }

        [DataMember]
        public string Nombre { get; set; }

        [DataMember]
        public bool factura { get; set; }

        [DataMember]
        public bool detalle { get; set; }

        [DataMember]
        public string icon { get; set; }

        [DataMember]
        public int opcion { get; set; }

        [DataMember]
        public long IdEstadoCuenta { get; set; }
    }


}
