﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;


namespace Softv.Entities
{
    [DataContract]
    [Serializable]
   public class InfoCliente
    {
        [DataMember]
        public long ? Contrato { get; set; }


        [DataMember]
        public string  ContratoCompuesto { get; set; }


        [DataMember]
        public string Cliente { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public string password { get; set; }

        [DataMember]
        public string Serie { get; set; }

        [DataMember]
        public string token { get; set; }

    }
}
