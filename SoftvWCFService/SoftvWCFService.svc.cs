﻿
using Microsoft.IdentityModel.Tokens;
using Softv.Entities;
using SoftvWCFService.Contracts;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Net;
using System.Security.Claims;
using System.ServiceModel.Web;
using System.Text;
using System.Web.Script.Services;
using CrystalDecisions.CrystalReports.Engine;
using SoftvWCFService.Security;

namespace SoftvWCFService
{
    [ScriptService]
    public partial class SoftvWCFService : IUsuario, ITransferService
    {
        string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        Int32 GloIdCompania = 0;
        token _token = new token();

        #region Usuario
        public UsuarioSoftvEntity GetusuarioByUserAndPass(string Usuariox, string Pass)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {

                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    SqlCommand comando = new SqlCommand("Softv_UsuarioGetusuarioByUserAndPass");
                    comando.CommandType = CommandType.StoredProcedure;
                    comando.CommandTimeout = 0;
                    comando.Connection = connection;

                    SqlParameter par1 = new SqlParameter("@Usuario", SqlDbType.VarChar);
                    par1.Value = Usuariox;
                    par1.Direction = ParameterDirection.Input;
                    comando.Parameters.Add(par1);

                    SqlParameter par2 = new SqlParameter("@Password", SqlDbType.VarChar);
                    par2.Value = Pass;
                    par2.Direction = ParameterDirection.Input;
                    comando.Parameters.Add(par2);
                    UsuarioSoftvEntity entity_Usuario = new UsuarioSoftvEntity();

                    IDataReader rd = null;
                    try
                    {
                        if (connection.State == ConnectionState.Closed)
                            connection.Open();
                        rd = comando.ExecuteReader();
                        while (rd.Read())
                        {
                            entity_Usuario.Contrato = long.Parse(rd[0].ToString());
                            entity_Usuario.Login = rd[1].ToString();

                        }                      
                        
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Error Softv_UsuarioGetusuarioByUserAndPass " + ex.Message, ex);
                    }
                    finally
                    {
                        if (connection != null)
                            connection.Close();
                        if (rd != null)
                            rd.Close();
                    }
                    return entity_Usuario;
                }
            }

           
        }

       

        public List<DocumentosEntity> Getdocumentosdisponibles(long? contrato)
        {

            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                List<DocumentosEntity> lista = new List<DocumentosEntity>();
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    SqlCommand comando_ = new SqlCommand("FacturasFiscalesContrato");
                    comando_.CommandType = CommandType.StoredProcedure;
                    comando_.CommandTimeout = 0;
                    comando_.Connection = connection;

                    SqlParameter para = new SqlParameter("@Contrato", SqlDbType.BigInt);
                    para.Direction = ParameterDirection.Input;
                    para.Value = contrato;
                    comando_.Parameters.Add(para);
                    
                    IDataReader rd = null;
                    try
                    {
                        if (connection.State == ConnectionState.Closed)
                            connection.Open();
                        rd = comando_.ExecuteReader();

                        while (rd.Read())
                        {
                            DocumentosEntity u = new DocumentosEntity();
                            u.Ticket = rd[0].ToString();
                            u.Fecha = rd[1].ToString();
                            u.Importe = decimal.Parse(rd[2].ToString());
                            u.factura = true;
                            u.Nombre = "Factura digital del ticket";
                            u.icon = "images/file.svg";
                            u.opcion = 1;
                            lista.Add(u);

                            DocumentosEntity uu = new DocumentosEntity();
                            uu.Ticket = "";
                            uu.Fecha = "";
                            uu.Importe = null;
                            uu.factura = true;
                            uu.Nombre = "Archivo CDFI";
                            uu.icon = "images/xml.svg";
                            uu.opcion = 2;
                            lista.Add(uu);



                        }
                        if (rd != null)
                            rd.Close();


                        //SqlCommand comando = new SqlCommand("tieneEdoCuenta");
                        //comando.CommandType = CommandType.StoredProcedure;
                        //comando.CommandTimeout = 0;
                        //comando.Connection = connection;

                        //SqlParameter par4 = new SqlParameter("@tieneEdoCuenta", SqlDbType.Bit);
                        //par4.Direction = ParameterDirection.Output;
                        //comando.Parameters.Add(par4);

                        //SqlParameter par5 = new SqlParameter("@idEstadoCuenta", SqlDbType.BigInt);
                        //par5.Direction = ParameterDirection.Output;
                        //comando.Parameters.Add(par5);

                        //SqlParameter par3 = new SqlParameter("@contrato", SqlDbType.BigInt);
                        //par3.Value = contrato;
                        //par3.Direction = ParameterDirection.Input;
                        //comando.Parameters.Add(par3);
                        //comando.ExecuteNonQuery();

                        //bool tiene = bool.Parse(par4.Value.ToString());
                        //if (tiene)
                        //{
                        //    DocumentosEntity t = new DocumentosEntity();
                        //    t.Ticket = "";
                        //    t.Fecha = "";
                        //    t.Importe = null;
                        //    t.factura = true;
                        //    t.Nombre = "Estado de cuenta";
                        //    t.opcion = 3;
                        //    t.icon = "images/pdf.svg";
                        //    lista.Add(t);
                        //}
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Error getting data ConRelClienteObs " + ex.Message, ex);
                    }
                    finally
                    {
                        if (connection != null)
                            connection.Close();
                    }
                }
                return lista;
            }

        }



        public int? GetDeletedocumentos(int? id, string nombre)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                string apppath = System.AppDomain.CurrentDomain.BaseDirectory;
                string fileName = apppath + "/Reportes/" + nombre;
                if (File.Exists(fileName))
                {
                    try
                    {
                        File.Delete(fileName);
                        return 1;
                    }
                    catch
                    {
                        return 0;
                    }

                }
                else
                {
                    return 0;
                }
            }
        }


        public List<DocumentosEntity> GetEstadosDeCuentaContratoHistorial(long ? Contrato)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                List<DocumentosEntity> lista = new List<DocumentosEntity>();
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    SqlCommand comando_ = new SqlCommand("EstadosDeCuentaContratoHistorial");
                    comando_.CommandType = CommandType.StoredProcedure;
                    comando_.CommandTimeout = 0;
                    comando_.Connection = connection;

                    SqlParameter para = new SqlParameter("@Contrato", SqlDbType.BigInt);
                    para.Direction = ParameterDirection.Input;
                    para.Value = Contrato;
                    comando_.Parameters.Add(para);

                    IDataReader rd = null;
                    try
                    {
                        if (connection.State == ConnectionState.Closed)
                            connection.Open();
                        rd = comando_.ExecuteReader();

                        while (rd.Read())
                        {
                            DocumentosEntity d = new DocumentosEntity();
                            d.IdEstadoCuenta = long.Parse(rd[0].ToString());
                            d.Fecha = rd[1].ToString();
                            lista.Add(d);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Error getting data EstadosDeCuentaContratoHistorial " + ex.Message, ex);
                    }
                    finally
                    {
                        if (connection != null)
                            connection.Close();
                        if (rd != null)
                            rd.Close();
                    }
                }

                return lista;
            }
        }

        public RemoteFileInfo DownloadFile(long? contrato, int? op)
        {
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }
            else
            {
                RemoteFileInfo result = new RemoteFileInfo();
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    DocumentosEntity u = new DocumentosEntity();
                    string apppath = System.AppDomain.CurrentDomain.BaseDirectory;

                    SqlCommand comando_ = new SqlCommand("FacturasFiscalesContrato");
                    comando_.CommandType = CommandType.StoredProcedure;
                    comando_.CommandTimeout = 0;
                    comando_.Connection = connection;

                    SqlParameter para = new SqlParameter("@Contrato", SqlDbType.BigInt);
                    para.Direction = ParameterDirection.Input;
                    para.Value = contrato;
                    comando_.Parameters.Add(para);
                    
                    IDataReader rd = null;
                    try
                    {
                        if (connection.State == ConnectionState.Closed)
                            connection.Open();
                        rd = comando_.ExecuteReader();

                        while (rd.Read())
                        {


                            if (op == 1)
                            {
                                try
                                {
                                    string name = Guid.NewGuid().ToString() + ".pdf";
                                    byte[] bytes = (byte[])rd[3];
                                    result.FileName = name;
                                    result.Length = bytes.Length;
                                    result.FileByteStream = Convert.ToBase64String(bytes);
                                }
                                catch (Exception ex)
                                {
                                    throw new Exception("Error No se pudo generar el archivo " + ex.Message, ex);
                                }
                            }

                            if (op == 2)
                            {
                                try
                                {
                                    string name = Guid.NewGuid().ToString() + ".xml";
                                    string fileName = apppath + "/Reportes/" + name;
                                    byte[] bytes = (byte[])rd[4];
                                    FileStream fs = new FileStream(fileName, FileMode.OpenOrCreate);
                                    fs.Write(bytes, 0, bytes.Length);
                                    fs.Close();

                                    string xmlString = System.Text.UTF8Encoding.UTF8.GetString(bytes);

                                    result.FileName = name;
                                    result.Length = bytes.Length;
                                    //result.FileByteStream = xmlString;
                                    result.FileByteStream = Convert.ToBase64String(bytes);

                                }
                                catch (Exception ex)
                                {
                                    throw new Exception("Error No se pudo generar el archivo " + ex.Message, ex);
                                }

                            }


                        }

                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Error getting data ConRelClienteObs " + ex.Message, ex);
                    }
                    finally
                    {
                        if (connection != null)
                            connection.Close();
                        if (rd != null)
                            rd.Close();
                    }



                    if (op == 3)
                    {

                        try
                        {

                            if (connection.State == ConnectionState.Closed)
                                connection.Open();

                            string name = Guid.NewGuid().ToString() + ".pdf";
                            string fileName = apppath + "/Reportes/" + name;
                            string rutarpts = apppath + "/Reportes/";
                            u.Nombre = name;
                            SqlCommand comando = new SqlCommand("tieneEdoCuenta");
                            comando.CommandType = CommandType.StoredProcedure;
                            comando.CommandTimeout = 0;
                            comando.Connection = connection;

                            SqlParameter par4 = new SqlParameter("@tieneEdoCuenta", SqlDbType.Bit);
                            par4.Direction = ParameterDirection.Output;
                            comando.Parameters.Add(par4);

                            SqlParameter par5 = new SqlParameter("@idEstadoCuenta", SqlDbType.BigInt);
                            par5.Direction = ParameterDirection.Output;
                            comando.Parameters.Add(par5);

                            SqlParameter par3 = new SqlParameter("@contrato", SqlDbType.BigInt);
                            par3.Value = contrato;
                            par3.Direction = ParameterDirection.Input;
                            comando.Parameters.Add(par3);
                            comando.ExecuteNonQuery();

                            long id = long.Parse(par5.Value.ToString());

                            ConEstadoCuentaCorreo(contrato, DateTime.Now, id, connection, rutarpts, fileName);

                            byte[] pdfBytes = File.ReadAllBytes(fileName);
                            result.FileName = name;
                            result.Length = 0;
                            result.FileByteStream = Convert.ToBase64String(pdfBytes);

                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error getting data ConRelClienteObs " + ex.Message, ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                            if (rd != null)
                                rd.Close();
                        }

                    }
                }

                return result;
            }
        }
           

        public  DameSessionWEntity GetAuthentication(string usuario, String password, string IpMachine)
        {
            DameSessionWEntity session = new DameSessionWEntity();
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {

                SqlCommand comandoSql = new SqlCommand("exec Authenticate @usuario,@password", connection);
                comandoSql.Parameters.AddWithValue("@usuario", usuario);
                comandoSql.Parameters.AddWithValue("@password", password);
                IDataReader rd = null;
                try
                {
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();
                    rd = comandoSql.ExecuteReader();

                    while (rd.Read())
                    {
                        session.usuario = rd[0].ToString();
                        session.contrato = long.Parse(rd[1].ToString());

                    }
                    rd.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error getting data DameSessionW " + ex.Message, ex);
                }
                finally
                {
                    if (connection != null)
                        connection.Close();
                    if (rd != null)
                        rd.Close();
                }
            }
            return session;
        }

    

        public DameSessionWEntity LogOn()
        {
            DameSessionWEntity objsession = new DameSessionWEntity();
            if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS")
            {
                return null;
            }            
            else 
            {
                string encodedUnamePwd = _token.GetEncodedCredentialsFromHeader();
                if (!string.IsNullOrEmpty(encodedUnamePwd))
                {                    
                    byte[] decodedBytes = null;                   
                    decodedBytes = Convert.FromBase64String(encodedUnamePwd);
                    string credentials = ASCIIEncoding.ASCII.GetString(decodedBytes);                    
                    string[] authParts = credentials.Split(':');
                    UsuarioSoftvEntity objUsr = GetusuarioByUserAndPass(authParts[0], authParts[1]);

                    if (objUsr != null)
                    {

                        objsession = GetAuthentication(authParts[0], authParts[1],"");
                        objsession.Codigo = _token.GenerateToken(authParts[0], 800);
                        objsession.Token = objsession.Codigo;
                        return objsession;
                    }
                    else
                    {                       
                        throw new WebFaultException<string>("Acceso no autorizado, favor de validar autenticación", HttpStatusCode.Unauthorized);
                    }
                }
            }

            return objsession;
        }


        #endregion


        public void ConEstadoCuentaCorreo(long? Contrato, DateTime Fecha, long ID, SqlConnection connection, string ruta, string fileName)
        {
            try
            {
                SqlConnection conexion = connection;

                ReportDocument reportDocument = new ReportDocument();
                String lineaPP;
                String lineaTR;
                String lineaC;
                String lineaO;

                //Para generar los códigos de barras
                SqlCommand comando = new SqlCommand("DameLineasCaptura");
                comando.CommandType = CommandType.StoredProcedure;
                comando.CommandTimeout = 0;
                comando.Connection = conexion;

                SqlParameter par1 = new SqlParameter("@idEstadoCuenta", SqlDbType.BigInt);
                par1.Value = ID;
                par1.Direction = ParameterDirection.Input;
                comando.Parameters.Add(par1);

                SqlParameter par2 = new SqlParameter("@contrato", SqlDbType.BigInt);
                par2.Value = Contrato;
                par2.Direction = ParameterDirection.Input;
                comando.Parameters.Add(par2);

                SqlParameter par3 = new SqlParameter("@lineaPP", SqlDbType.VarChar, 40);
                par3.Direction = ParameterDirection.Output;
                comando.Parameters.Add(par3);

                SqlParameter par4 = new SqlParameter("@lineaTR", SqlDbType.VarChar, 40);
                par4.Direction = ParameterDirection.Output;
                comando.Parameters.Add(par4);

                SqlParameter par5 = new SqlParameter("@lineaC", SqlDbType.VarChar, 40);
                par5.Direction = ParameterDirection.Output;
                comando.Parameters.Add(par5);

                SqlParameter par6 = new SqlParameter("@lineaO", SqlDbType.VarChar, 40);
                par6.Direction = ParameterDirection.Output;
                comando.Parameters.Add(par6);

                comando.ExecuteNonQuery();
                lineaPP = par3.Value.ToString();
                lineaTR = par4.Value.ToString();
                lineaC = par5.Value.ToString();
                lineaO = par6.Value.ToString();
                Byte[] imgPP = GeneraCodeBar(lineaPP);
                Byte[] imgTR = GeneraCodeBar(lineaTR);
                Byte[] imgC = GeneraCodeBar(lineaC);
                Byte[] imgO = GeneraCodeBar(lineaO);
                //Fin para generar estado de cuenta
                SqlCommand strSQL = new SqlCommand("ReporteEstadoCuentaNuevo");
                strSQL.Connection = conexion;
                strSQL.CommandType = CommandType.StoredProcedure;
                strSQL.CommandTimeout = 0;

                SqlParameter par10 = new SqlParameter("@ID", SqlDbType.BigInt);
                par10.Value = ID;
                par10.Direction = ParameterDirection.Input;
                strSQL.Parameters.Add(par10);

                SqlParameter par20 = new SqlParameter("@CONTRATO", SqlDbType.BigInt);
                par20.Value = Contrato;
                par20.Direction = ParameterDirection.Input;
                strSQL.Parameters.Add(par20);

                SqlParameter par30 = new SqlParameter("@OP", SqlDbType.Int);
                par30.Value = 1;
                par30.Direction = ParameterDirection.Input;
                strSQL.Parameters.Add(par30);

                SqlParameter par40 = new SqlParameter("@idcompania", SqlDbType.Int);
                par40.Value = GloIdCompania;
                par40.Direction = ParameterDirection.Input;
                strSQL.Parameters.Add(par40);

                SqlParameter par50 = new SqlParameter("@imgPP", SqlDbType.Image);
                par50.Value = imgPP;
                par50.Direction = ParameterDirection.Input;
                strSQL.Parameters.Add(par50);

                SqlParameter par60 = new SqlParameter("@imgTR", SqlDbType.Image);
                par60.Value = imgTR;
                par60.Direction = ParameterDirection.Input;
                strSQL.Parameters.Add(par60);

                SqlParameter par70 = new SqlParameter("@imgC", SqlDbType.Image);
                par70.Value = imgC;
                par70.Direction = ParameterDirection.Input;
                strSQL.Parameters.Add(par70);

                SqlParameter par71 = new SqlParameter("@imgO", SqlDbType.Image);
                par71.Value = imgO;
                par71.Direction = ParameterDirection.Input;
                strSQL.Parameters.Add(par71);

                SqlDataAdapter dataAdapter = new SqlDataAdapter(strSQL);
                DataSet dataSet = new DataSet();
                DataTable dataTable = new DataTable();
                dataAdapter.Fill(dataSet);
                dataTable = MuestraGeneral(connection);
                dataSet.Tables[0].TableName = "EstadoCuenta";
                dataSet.Tables[1].TableName = "DetEstadoCuenta";
                dataSet.Tables.Add(dataTable);

                //dataSet.Tables[0].TableName = "EstadoCuenta";
                //dataSet.Tables[1].TableName = "DetEstadoCuenta";
                if (lineaPP.Length == 24)
                {
                    if (lineaPP.Substring(0, 6) == "440702")
                    {
                        reportDocument.Load(ruta + "EstadoCuentaInternet24.rpt");
                    }
                    else
                    {
                        reportDocument.Load(ruta + "EstadoCuenta24.rpt");
                    }
                }
                else if (lineaPP.Length == 26)
                {
                    if (lineaPP.Substring(0, 6) == "440702")
                    {
                        reportDocument.Load(ruta + "EstadoCuentaInternet.rpt");
                    }
                    else
                    {
                        reportDocument.Load(ruta + "EstadoCuenta.rpt");
                    }
                }
                reportDocument.SetDataSource(dataSet);
                reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, fileName);
            }
            catch (Exception ex)
            {

            }
        }


        public DataTable MuestraGeneral(SqlConnection connection)
        {
            SqlConnection conexion = connection;
            StringBuilder stringBuilder = new StringBuilder("EXEC MuestraGeneral");
            SqlDataAdapter dataAdapter = new SqlDataAdapter(stringBuilder.ToString(), conexion);
            DataTable dataTable = new DataTable();
            dataAdapter.Fill(dataTable);
            return dataTable;
        }


        public Byte[] GeneraCodeBar(String Data)
        {
            BarcodeLib.Barcode b = new BarcodeLib.Barcode();
            Byte[] imgBarCode = null;

            Int32 W = 350;
            Int32 H = 100;
            BarcodeLib.AlignmentPositions Align;
            Align = BarcodeLib.AlignmentPositions.CENTER;
            BarcodeLib.TYPE type;
            type = BarcodeLib.TYPE.CODE128;
            try
            {
                b.IncludeLabel = true;
                b.Alignment = Align;
                b.RotateFlipType = (RotateFlipType.RotateNoneFlipNone);
                //b.LabelPosition=LabelPosition.BOTTOMCENTER;
                Image img = b.Encode(type, Data, Color.Black, Color.White, W, H);
                //barcode.Width = barcode.Image.Width;
                //barcode.Height = barcode.Image.Height;
                Byte[] imgbites;
                imgbites = ImageToByte2(img);
                imgBarCode = imgbites;
                return imgBarCode;
            }
            catch (Exception ex)
            {

            }
            //return imgBarCode;
            return imgBarCode;
        }

        public Byte[] ImageToByte2(Image img)
        {
            Byte[] byteArray;
            using (MemoryStream stream = new MemoryStream())
            {
                img.Save(stream, System.Drawing.Imaging.ImageFormat.Jpeg);
                stream.Close();
                byteArray = stream.ToArray();
            }
            return byteArray;
        }

    }
}
