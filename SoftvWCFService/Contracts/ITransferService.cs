﻿using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Web;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface  ITransferService
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "DownloadFile", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        RemoteFileInfo DownloadFile(long? contrato, int? op);

      
    }

  

    
}