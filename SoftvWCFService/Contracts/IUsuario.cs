﻿

using Softv.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace SoftvWCFService.Contracts
{
    [AuthenticatingHeader]
    [ServiceContract]
    public interface IUsuario
    {

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetusuarioByUserAndPass", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        UsuarioSoftvEntity GetusuarioByUserAndPass(string Usuario, string Pass);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "LogOn", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        DameSessionWEntity LogOn();



        //[OperationContract]
        //[WebInvoke(Method = "*", UriTemplate = "Getdocumentos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        //List<DocumentosEntity> Getdocumentos(long? contrato, int ? op);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetDeletedocumentos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int? GetDeletedocumentos(int? id, string nombre);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "Getdocumentosdisponibles", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<DocumentosEntity> Getdocumentosdisponibles(long? contrato);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetEstadosDeCuentaContratoHistorial", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<DocumentosEntity> GetEstadosDeCuentaContratoHistorial(long? Contrato);
        
    }
}

